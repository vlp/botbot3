use std::process::Command;
use chrono::prelude::*;

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////  Gestion des réponses en fonction des actions

pub fn get_answer(botbot_phrase: String, name: &String, role: String) -> Result<String, String>{

    let local: DateTime<Local> = Local::now();
    let f_time=format!("{}",local.format("%Hh%M").to_string());
    let f_date=format!("{}",local.format("%Y-%m-%d").to_string());
    let f_dow=format!("{}",local.weekday().to_string());
    let arg_role=format!("{}",role);
    let param_role=format!("{},{},{},{}",f_time,f_date,f_dow,name);

    //println!("Role: {} | Message: {}", arg_role, botbot_phrase);

    let aichat_command = Command::new("aichat")
       .arg("-r")
       .arg(arg_role)   
       .arg(param_role)   
       .arg(botbot_phrase)
       .output()
       .expect("failed to execute aichat process");

    let aichat_answer = String::from_utf8_lossy(&aichat_command.stdout);

    //println!("AICHAT ANSWER: {:?}", aichat_command);

    Ok(aichat_answer.to_string())
}

pub fn get_stt(file_to_text: String) -> Result<String, String>{

    let stt_file_path = format!("{}/{}",crate::MATRIX_MEDIA_FOLDER,file_to_text);
    let stt_command = Command::new("/srv/botbot_python3.8_venv/scripts/stt.sh")
       .arg(stt_file_path.clone())
       .output()
       .expect("failed to execute stt process");

    let stt_answer = String::from_utf8_lossy(&stt_command.stdout);
    println!("DEBUG stt: {}, {}", stt_file_path, stt_answer.to_string());
    Ok(stt_answer.to_string())
}
